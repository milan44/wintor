package wintor

import (
	"bufio"
	"fmt"
	"net/http"
	"net/url"
	"os"
	"os/exec"
	"runtime"
	"strconv"
	"strings"
	"time"
)

type StartConf struct {
	ExePath        string
	StartupTimeout int64
	Verbose        bool
	RestartTor     bool
	TorrcFile      string
}

func Connect(conf StartConf) bool {
	if conf.RestartTor == false {
		if conf.Verbose {
			fmt.Println("Checking if tor is running...")
		}
		if isTorRunning() {
			if conf.Verbose {
				fmt.Println("Checking if tor is already running!")
			}
			return true
		}
		if conf.Verbose {
			fmt.Println("Checking if tor is not running!")
		}
	}
	Close()

	exe := conf.ExePath

	if runtime.GOOS != "windows" {
		if _commandExists("tor") {
			exe = "tor"
		} else {
			return false
		}
	}
	if _, err := os.Stat(exe); os.IsNotExist(err) {
		fmt.Println("Executable not found")
		return false
	}

	now := time.Now()
	start := now.Unix()

	running := false

	var args []string

	if conf.TorrcFile != "" {
		args = append(args, "-f", conf.TorrcFile)
	}

	if conf.Verbose {
		fmt.Println("Running startup command '" + exe + "'")
	}

	go func() {
		cmd := exec.Command(exe, args...)
		stdout, _ := cmd.StdoutPipe()
		stderr, _ := cmd.StderrPipe()
		error := cmd.Start()

		if error != nil {
			if conf.Verbose {
				fmt.Println(error)
				start = 0
				return
			}
		}

		go func() {
			scanner := bufio.NewScanner(stderr)
			for scanner.Scan() {
				line := scanner.Text()
				if conf.Verbose {
					fmt.Println("[STDERR] " + line)
				}
			}
		}()

		scanner := bufio.NewScanner(stdout)
		for scanner.Scan() {
			line := scanner.Text()
			if conf.Verbose {
				fmt.Println(line)
			}

			if strings.Contains(line, "Bootstrapped 100% (done)") {
				running = true
				return
			}
		}
		scanner2 := bufio.NewScanner(stderr)
		for scanner2.Scan() {
			line := scanner2.Text()

			fmt.Println(line)
		}
	}()

	for {
		now := time.Now()
		end := now.Unix()

		if end-start > conf.StartupTimeout && !running {
			Close()
			break
		}

		time.Sleep(1 * time.Second)

		if running {
			break
		}
	}

	return running
}

func isTorRunning() bool {
	if runtime.GOOS != "windows" {
		out, err := exec.Command("pgrep", "tor").CombinedOutput()
		if err != nil {
			return true
		}

		if string(out) != "" {
			return true
		}
		return false
	}
	cmd := exec.Command("tasklist")
	out, err := cmd.CombinedOutput()
	if err != nil {
		return false
	}

	lines := strings.Split(string(out), "\n")
	for _, line := range lines {
		if strings.HasPrefix(line, "tor.exe") {
			return true
		}
	}
	return false
}

func _commandExists(cmd string) bool {
	_, err := exec.LookPath(cmd)
	return err == nil
}

func Close() {
	stopTor := exec.Command("taskkill", "/IM", "tor.exe", "/F")
	_ = stopTor.Run()
}

type ClientConf struct {
	IP   string
	Port int
}

func GetClient(conf ClientConf) *http.Client {
	if conf.Port == 0 {
		conf.Port = 9050
	}

	if conf.IP == "" {
		conf.IP = "127.0.0.1"
	}

	proxyUrl, _ := url.Parse("socks5://" + conf.IP + ":" + strconv.Itoa(conf.Port))
	client := &http.Client{Transport: &http.Transport{Proxy: http.ProxyURL(proxyUrl)}}

	return client
}
