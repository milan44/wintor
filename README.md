# wintor

This package enables http requests through the tor network.

### Example GET request

```go
func main() {
    wintor.connect(wintor.StartConf {
        ExePath: "C:\\path\\to\\my\\tor.exe",
        StartupTimeout: 60, // Timeout after which it will stop if connection failed
        Verbose: true, // If it should print out everything the tor executable says
        RestartTor: false, // If tor should get restarted if its already running
        TorrcFile: "C:\\path\\to\\my\\.torrc"
    })

    client := wintor.getClient(wintor.ClientConf{
        Port: 9050,
        IP: "127.0.0.1",
    })

    resp, err := client.Get("http://example.com")
}
```